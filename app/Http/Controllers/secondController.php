<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use Illuminate\Support\Facades\DB;
use App\process;
use App\Task;

class secondController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	$history = Task::all()->where('user_id',Auth::user()->id);
        $process = DB::table('processes')->where('user_id',Auth::user()->id)->get();
    	return view('history',compact('history','process'));
    }
    public function ajax()
    {
        return $process = DB::table('processes')->where('user_id',Auth::user()->id)->get();
    }
    public function tampil()
    {
        return view('halamanajax');
    }
    public function calendar()
    {
        return view('calendar');
    }
    public function mailer(Request $request)
    {
        $data = array('status' => $request->status,'kegiatan'=>$request->kegiatan,'from'=>Auth::user()->name);
        Mail::to($request->email)->send(new SendMail($data));

        return redirect('/home');
    }
    
}
